const http = require('http');
const fs = require('fs');
const url = require('url');
const uuid = require('uuid');




const jsonData = {
    "slideshow": {
        "author": "Yours Truly",
        "date": "date of publication",
        "slides": [
            {
                "title": "Wake up to WonderWidgets!",
                "type": "all"
            },
            {
                "items": [
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                ],
                "title": "Overview",
                "type": "all"
            }
        ],
        "title": "Sample Slide Show"
    }
}


function serverStart() {
    try {
        const port = 8080
        http.createServer((request, responce) => {
           
            let precentURl = decodeURI(request.url)
            let path = url.parse(request.url).path
            let check = precentURl.split('/')
            if (check.length > 2) {
                path = `/${check[1]}`
            }

            switch (request.method) {
                case 'GET':
                    switch (path) {
                        case '/html':
                            fs.readFile('server/index.html', 'utf-8', (err, data) => {
                                if (err) {
                                    throw err
                                } else {
                                    responce.writeHead(200, { 'Content-Type': 'text/html' });
                                    console.log("in home page")
                                    responce.write((data.toString()))
                                    responce.end()
                                }
                            })
                            break;
                        case '/json':
                            responce.writeHead(200, { 'Content-Type': 'application/json' });
                            responce.write(JSON.stringify(jsonData))
                            responce.end()
                            break;
                        case '/uuid':
                            let uid = uuid.v4();
                            console.log(uid.toString())
                            responce.write(JSON.stringify({'uuid':uid}))
                            responce.end()
                            break;
                        case '/status':
                            console.log("status")
                            if (check.length > 2) {
                                responce.write(`${check[2]} isthe status`)
                            } else {
                                responce.write(`yoy are on statuc screen`)
                            }
                            responce.end()
                            break;
                        case '/delay':
                            if (check.length > 2) {
                                let delay = decodeURI(request.url).split('/')[2]
                                delay = delay.substring(1, delay.length - 1)
                                console.log(delay)
                                setTimeout(() => {
                                    responce.write(`${delay} is the delay`)
                                    responce.end()
                                }, delay * 1000);
                            } else {
                                responce.write("please add delay in /delay/{1}")
                                responce.end()
                            }

                            break;

                        default:
                            responce.write('404 Not Found')
                            responce.end()
                            break;
                    }
                    break;
                default:
                    responce.writeHead(404, { 'Content-Type': 'text/plain' });
                    console.log("not found")
                    responce.write('404 Not Found')
                    responce.end()
                    break;
            }
        }).listen(port, (err) => {
            console.log("start")
            if (err) {
                console.log(err)
            }
        })
    } catch (error) {
        console.log(error)
    }
}

serverStart()